$(document).ready(function() {
	var nuevaImagen;
	var bandera = 1;
	$(".imageClick").click(function(event){
		if (bandera) {
			bandera = 0;
			nuevaImagen = "../css/img/hide2.png";
		}
		else{
			bandera = 1;
			nuevaImagen = "../css/img/hide1.png";
		}
		event.preventDefault();
		$(this).fadeOut(120,function(){
			$(".imageClick").attr("src", nuevaImagen).fadeIn(120);
		});
	});

	// instead of import {} from 'leaflet-geosearch', use the `window` global
    var GeoSearchControl = window.GeoSearch.GeoSearchControl;
    var OpenStreetMapProvider = window.GeoSearch.OpenStreetMapProvider;
 
    // remaining is the same as in the docs, accept for the var instead of const declarations
    var provider = new OpenStreetMapProvider();
	var searchControl = new GeoSearchControl({
        provider: provider,
        style: 'bar',                   // optional: bar|button  - default button
		marker: {                                           // optional: L.Marker    - default L.Icon.Default
		    icon: new L.Icon.Default(),
		    draggable: true,
		},
		popupFormat: ({ query, result }) => result.label,   // optional: function    - default returns result label
		maxMarkers: 1,                                      // optional: number      - default 1
		retainZoomLevel: false,                             // optional: true|false  - default false
		animateZoom: true,                                  // optional: true|false  - default true
		autoClose: true,                                   // optional: true|false  - default false
		searchLabel: 'Ingrese dirección',                       // optional: string      - default 'Enter address'
		keepResult: false

    });


	var mymap = L.map('mapid').setView([51.505, -0.09], 13);
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> ' +
                 'contributors, ' +
                 '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                 'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18
	}).addTo(mymap);

	mymap.addControl(searchControl);

	
	var marker; 

	function onMapClick(e) {
		var latitud = e.latlng.lat;
		var longitud = e.latlng.lng;
		actualizarLatLng(latitud,longitud);
		mymap.removeLayer(marker);
		marker = new L.marker(e.latlng, {draggable:'true'})
            .addTo(mymap)
        mymap.addLayer(marker);
        marker.openPopup();

  		marker.on("dragend",function(e){
            var chagedPos = e.target.getLatLng();
            
            actualizarLatLng(chagedPos.lat,chagedPos.lng);
			cargarLatLng(chagedPos.lat,chagedPos.lng);
        });

        cargarLatLng(e.latlng.lat,e.latlng.lng);

	}
	
	mymap.on('click', onMapClick);


	if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(posicion => {
            const { latitude, longitude } = posicion.coords;
 
	            marker = new L.marker([latitude, longitude], {draggable:'true'})
	                .addTo(mymap)
	                .bindPopup("¿Estás aquí?");
	            mymap.addLayer(marker);
		        marker.openPopup();

	        marker.on("dragend",function(e){
	            var chagedPos = e.target.getLatLng();

				actualizarLatLng(chagedPos.lat,chagedPos.lng);
				cargarLatLng(chagedPos.lat,chagedPos.lng);
	        });
 
            mymap.panTo([latitude, longitude]);//centra en popup

            actualizarLatLng(latitude, longitude)
            cargarLatLng(latitude,longitude);
        });
    }

});

function actualizarLatLng(latitud, longitud){
	document.getElementById("lat").value = latitud;
	document.getElementById("long").value = longitud;
	document.getElementById("latitud").value = latitud;
	document.getElementById("longitud").value = longitud;
}

async function cargarLatLng(latitud, longitud) {
    var JSON_Posicion = await reverseGeocoding(latitud, longitud);

    document.getElementById("idpais").value = JSON_Posicion.features['0'].properties.address.country;
	document.getElementById("idprov").value = JSON_Posicion.features['0'].properties.address.state;
    document.getElementById("idciudad").value = JSON_Posicion.features['0'].properties.address.state_district;
    document.getElementById("idcalle").value = JSON_Posicion.features['0'].properties.address.road;
    // document.getElementById("idnum").value = JSON_Posicion.features['0'].properties.address.house_number;
}
 
async function reverseGeocoding(latitud, longitud) {
    var URL = 'https://nominatim.openstreetmap.org/reverse?format=geojson&lat=' + latitud + '&lon=' + longitud;
    return fetch(URL).then(function(response) {
        return response.json();
    });
}

function validad_usuario(){
        
    $.ajax({
        method: "POST",
        url: "/app/usuarios/validar_email",
        data: { email: $('#idemail').val()}
    }).done(function( msg ) {
        // alert( "dato json tipo texto: " + msg );
        var data=JSON.parse(msg);
        
        if(data.warning){
            $("#comprobar").attr("src","../css/img/X.jpg");
        }
        
        if(data.success){
            $("#comprobar").attr("src","../css/img/check.png");
        }        
        
        });               
}

$(function(){
	

});

