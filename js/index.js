var mymap;
var marker; 
var nueva_latitud;
var nueva_longitud;
var marcadores = [];
$(document).ready(function() {

	// instead of import {} from 'leaflet-geosearch', use the `window` global
	var GeoSearchControl = window.GeoSearch.GeoSearchControl;
	var OpenStreetMapProvider = window.GeoSearch.OpenStreetMapProvider;
	
    // remaining is the same as in the docs, accept for the var instead of const declarations
    var provider = new OpenStreetMapProvider();
    var searchControl = new GeoSearchControl({
    	provider: provider,
        style: 'bar',                   // optional: bar|button  - default button
		marker: {                                           // optional: L.Marker    - default L.Icon.Default
			icon: new L.Icon.Default(),
			draggable: true,
		},
		popupFormat: ({ query, result }) => result.label,   // optional: function    - default returns result label
		maxMarkers: 1,                                      // optional: number      - default 1
		retainZoomLevel: false,                             // optional: true|false  - default false
		animateZoom: true,                                  // optional: true|false  - default true
		autoClose: true,                                   // optional: true|false  - default false
		searchLabel: 'Ingrese dirección',                       // optional: string      - default 'Enter address'
		keepResult: false

	});


    mymap = L.map('mapid').setView([51.505, -0.09], 13);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    	attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> ' +
    	'contributors, ' +
    	'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    	'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    	maxZoom: 18
    }).addTo(mymap);

    mymap.addControl(searchControl);

    
    

    // function onMapClick(e) {
    // 	var latitud = e.latlng.lat;
    // 	var longitud = e.latlng.lng;
    // 	actualizarLatLng(latitud,longitud);
    // 	mymap.removeLayer(marker);
    // 	marker = new L.marker(e.latlng, {draggable:'true'})
    // 	.addTo(mymap);
    // 	mymap.addLayer(marker);
    // 	marker.openPopup();

    // 	marker.on("dragend",function(e){
    // 		var chagedPos = e.target.getLatLng();
    		
    // 		actualizarLatLng(chagedPos.lat,chagedPos.lng);
    // 	});

    // }
    // mymap.on('click', onMapClick);


    if ("geolocation" in navigator) {
    	navigator.geolocation.getCurrentPosition(posicion => {
    		const { latitude, longitude } = posicion.coords;
    		
    		marker = new L.marker([latitude, longitude], {draggable:'true'})
    		.addTo(mymap)
    		.bindPopup("¿Estás aquí?");
    		mymap.addLayer(marker);
    		marker.openPopup();
            marcadores.push(marker);

    		marker.on("dragend",function(e){
    			var chagedPos = e.target.getLatLng();

    			actualizarLatLng(chagedPos.lat,chagedPos.lng);
    		});
    		
            mymap.panTo([latitude, longitude]);//centra en popup

            actualizarLatLng(latitude, longitude)
        });
    }  

    $('#tabla').load('tabla');
});

function actualizarLatLng(latitud, longitud){
    nueva_latitud = latitud;
    nueva_longitud = longitud;
}

function cargarLatLong(datos){
    d=datos.split('||');

    $('#iD').val(d[0]);
    $('#latD').val(d[5]);
    $('#longD').val(d[6]);

    marker = new L.marker([d[5],d[6]], {draggable:'true'})
    .bindPopup("<div class='contenedor' id='d[0]'>"+d[2]+" "+d[3]+"<button class='buttons' onclick='cargarDialogUsuarios(`"+datos+"`)'>Información</button><br><button class='buttons' onclick='verImagenes(`"+d[0]+"`)'>Imágenes</button><br><button class='buttons' onclick='verLugaresCercanos(`"+datos+"`)'>Información</button></div>");
    mymap.addLayer(marker);//new
    marker.openPopup();    
    marcadores.push(marker);

    actualizarLatLng(d[5],d[6]);
}

function cargarDialogUsuarios(datos) {
    d = datos.split('||');

    $('#idU').html(d[0]);
    $('#emailU').html(d[1]);
    $('#nombreU').html(d[2]);
    $('#apellidoU').html(d[3]);
    $('#telU').html(d[4]);
    $('#latU').html(d[5]);
    $('#longU').html(d[6]);
    $('#generoU').html(d[7]);
    $('#fecha_nacU').html(d[8]);
    $('#webU').html(d[9]);
    $('#idpaisU').html(d[10]);
    $('#idprovU').html(d[11]);
    $('#idciudadU').html(d[12]);
    $('#idcalleU').html(d[13]);


    var opt = {
        autoOpen: true,
        zIndex: -20000,
        modal: true,
        width: 400,
        height: 'auto',
        resizable: false,
        show: {
            effect: 'slide',
            duration: 200
        },
        hide: {
            effect: 'slide',
            duration: 200
        },
    };

    $("#dialogU").dialog(opt).dialog("open");

}
function cargarDialogLoc(datos){
    d=datos.split('||');

    $('#iD').val(d[0]);
    $('#latD').val(nueva_latitud);
    $('#longD').val(nueva_longitud);

    cargarLatLng(nueva_latitud,nueva_longitud);

    var opt = {
        autoOpen: true,
        zIndex: -20000,
        modal: true,
        width: 550,
        height: 'auto',
        resizable: false,
        show: {
            effect: 'slide',
            duration: 200
        },
        hide: {
            effect: 'slide',
            duration: 200
        },
    };

    $("#dialogLoc").dialog(opt).dialog("open");   
}

async function cargarLatLng(latitud, longitud) {
    var JSON_Posicion = await reverseGeocoding(latitud, longitud);

    document.getElementById("idpais").value = JSON_Posicion.features['0'].properties.address.country;
    document.getElementById("idprov").value = JSON_Posicion.features['0'].properties.address.state;
    document.getElementById("idciudad").value = JSON_Posicion.features['0'].properties.address.state_district;
    document.getElementById("idcalle").value = JSON_Posicion.features['0'].properties.address.road;
    // document.getElementById("idnum").value = JSON_Posicion.features['0'].properties.address.house_number;
}
async function reverseGeocoding(latitud, longitud) {
    var URL = 'https://nominatim.openstreetmap.org/reverse?format=geojson&lat=' + latitud + '&lon=' + longitud;
    return fetch(URL).then(function(response) {
        return response.json();
    });
}

function actualizarLoc(){
    event.preventDefault();
    $.ajax({
        url: "/app/usuarios/actualizarLoc",
        type: "POST",
        data: $('#locD').serialize(),
        success: function(data){
            $('#tabla').load('tabla');
            alert("Actualizado con exito :)");
            $("#dialogLoc").dialog('close');
        },
        error: function(){
            alert("Fallo el servidor xD");
        }
    });
}
function cargarDialog(datos){   
    d=datos.split('||');

    $('#idD').val(d[0]);
    $('#nombreD').val(d[2]);
    $('#apellidoD').val(d[3]);
    $('#emailD').val(d[1]);
    $('#telefonoD').val(d[4]);
    

    var opt = {
        autoOpen: true,
        zIndex: -20000,
        modal: true,
        width: 300,
        height: 'auto',
        resizable: false,
        show: {
            effect: 'slide',
            duration: 200
        },
        hide: {
            effect: 'slide',
            duration: 200
        },
    };

    $("#dialog").dialog(opt).dialog("open"); 
}
function actualizarDatos(){
    event.preventDefault();
    $.ajax({
        url: "/app/usuarios/actualizarDatos",
        type: "POST",
        data: $('#formD').serialize(),
        success: function(data){
            $('#tabla').load('tabla');
            alert("Actualizado con exito :)");
            $("#dialog").dialog('close');
        },
        error: function(){
            alert("Fallo el servidor xD");
        }
    });
}

function validar_email_dialog(){
    $.ajax({
        method: "POST",
        url: "/app/usuarios/validar_email_dialog",
        data: { email: $('#emailD').val()}
    }).done(function( msg ) {
        // alert( "dato json tipo texto: " + msg );
        var data=JSON.parse(msg);
        
        if(data.warning){
            alert(data.message);
        }
        
        if(data.success){
            alert(data.message);
        }        
        
    });               
}

function verImagenes(idUsuario) {
    $.ajax({
        method: "POST",
        url: "/app/usuarios/crearRutaCarpetas",
        data: { idUsuario: idUsuario }
    }).done(function(msg) {
        var rutaCarpeta = JSON.parse(msg);

        var config = {};

        config.rememberLastFolder = false;

        config.displayFoldersPanel = false;

        config.startupPath = "Images:/" + rutaCarpeta;

        CKFinder.modal(config);
    });
}

// NEW
function verLugaresCercanos(datos){
    cargarLatLong(datos);
    setTimeout(function() {
        mostrarLugares(datos);
    }, 100);
}
function mostrarLugares(datos) {

    datosUsuario = datos.split('||');   

    mymap.setView([datosUsuario[5], datosUsuario[6]], 13);
    featureLayer = L.geoJson(undefined, {
        pointToLayer: function(f, latLng) {
            return L.circleMarker(latLng, {
                radius: 4,
                opacity: 1,
                fillOpacity: 0.5
            })
            .bindPopup('<li><span class="maki-icon ' + f.properties.maki + '"></span>' + f.properties.name + '</li>');
        }
    }).addTo(mymap);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);

    var pois = L.underneath('http://{s}.tiles.mapbox.com/v4/mapbox.mapbox-streets-v6/' +
        '{z}/{x}/{y}.vector.pbf?access_token=pk.eyJ1IjoianVsaW94ZCIsImEiOiJjazJwYnFtenAwMXozM2Jxb3dobjJ1ejVoIn0.Eth1AjnzgN7VJT_sGlZy8w', mymap, {
            layers: ['poi_label'],
            zoomIn: 2
        });

    mymap.on('click', function(e) {

        var results = [],
        content = '<h3>POI List</h3> <ul>',
        showResults = function(results) {
            featureLayer.addData(results);
            for (var i = 0; i < results.length; i++) {
                var f = results[i],
                c = f.geometry.coordinates;
                content += '<li><span class="maki-icon ' +
                f.properties.maki + '"></span>' +
                f.properties.name +
                '</li>';
            }

            content += '</ul>';

            L.popup()
            .setLatLng([datosUsuario[5], datosUsuario[6]])
            .setContent(content)
            .openOn(mymap);
        };

        featureLayer.clearLayers();
        pois.query([datosUsuario[5], datosUsuario[6]], function(err, results) {
            if (results.length > 0) {
                results = results.splice(0, 15);
                showResults(results);
            }
        }, null, { radius: 600 });
    });
    
    L.popup()
    .setLatLng(mymap.getCenter())
    .setContent('<h4>Se encontraron puntos de interés cerca de ' + datosUsuario[2] + ' ' + datosUsuario[3] + '  en un radio de 600m</h4>Haga un clic sobre el mapa para ver las localizaciones')
    .openOn(mymap);

}

function borrarMarkers(){
    marcadores.forEach(element => mymap.removeLayer(element));
}