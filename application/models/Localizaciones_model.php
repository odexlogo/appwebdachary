<?php

class Localizaciones_model extends CI_Model {

    public $latitud; 
    public $longitud; 
    public $ciudad;
    public $calle;
    public $pais;
    public $provincia;

    public function nuevo_loc($latitud,$longitud,$ciudad,$calle,$pais,$provincia)
    {
        $this->latitud=$latitud; 
        $this->longitud=$longitud; 
        $this->ciudad=$ciudad;
        $this->calle=$calle;
        $this->pais=$pais;
        $this->provincia=$provincia;

        $resul=$this->db->insert('localizaciones', $this);
        
        return $resul;

    }

    public function get_loc($latitud,$longitud)
    {
        $this->db->where('latitud', $latitud);
        $this->db->where('longitud', $longitud);
        $query = $this->db->get('localizaciones');
        return $query->result();
    }

}

