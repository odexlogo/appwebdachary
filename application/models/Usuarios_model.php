<?php

class Usuarios_model extends CI_Model {

    public $email;
    public $pass;
    public $latitud;
    public $longitud;

    public function get_usuario_by_email($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('usuarios');
        return $query->result();
    }

    public function get_usuarios(){
        $query = $this->db->get('usuarios');
        return $query->result();
    }

    public function get_tablas(){
        $query = $this->db->get('tablas');
        return $query->result();
    }
    public function validar_estado($id){
        $id; 
        $this->db->set('estado', true);
        $this->db->where('id', $id);
        $this->db->update('usuarios');
    }

    public function nuevo_usuario($email,$pass,$latitud,$longitud)
    {
        $this->email=$email; 
        $this->pass=$pass;
        $this->latitud=$latitud;
        $this->longitud=$longitud;
        $resul=$this->db->insert('usuarios', $this);

        
        return $resul;
        
    }

    public function actualizar_email($id,$email){
        $array = array(
            'email' => $email
        );
        $this->db->set($array);
        $this->db->where('id', $id);
        $resul = $this->db->update('usuarios');

        return $resul;
    }

    public function actualizar_usuarioLoc($id,$latitud,$longitud){
        $array = array(
            'latitud' => $latitud,
            'longitud' => $longitud
        );
        $this->db->set($array);
        $this->db->where('id', $id);
        $resul = $this->db->update('usuarios');

        return $resul;
    }

}

