<?php

class Personas_model extends CI_Model {

    public $id;
    public $nombre;
    public $apellido;
    public $fecha_nac;
    public $web;
    public $tel;
    public $genero;

    public function get_persona_by_id($id){
        $this->db->where('id', $id);
        $query = $this->db->get('personas');
        return $query->result();
    }

    public function get_personas(){
        $query = $this->db->get('personas');
        return $query->result();
    }
    public function nueva_persona($id,$nombre,$apellido,$fecha_nac,$web,$tel,$genero)
    {       
        $this->id=$id;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->fecha_nac=$fecha_nac;
        $this->web=$web;
        $this->tel=$tel;
        $this->genero=$genero;
        
        $resul=$this->db->insert('personas', $this);
        
        return $resul;

    }

    public function actualizar_persona($id,$nombre,$apellido,$tel){
        $array = array(
            'nombre' => $nombre,
            'apellido' => $apellido,
            'tel' => $tel
        );
        $this->db->set($array);
        $this->db->where('id', $id);
        $resul = $this->db->update('personas');

        return $resul;
    }
}

