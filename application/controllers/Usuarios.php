<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function registrar(){
		$this->load->view('formulario_registro');
	}
	public function login(){
		$this->load->view('formulario_login');
	}
	public function inicio(){	
		$this->load->view('index');
	}
	public function tabla(){
		$this->load->view('tabla');
	}

	public function actualizarLoc(){
		$this->load->model('localizaciones_model','localizacion');
		$this->load->model('usuarios_model','usuario');

		$id=$this->input->post('iD');
		$lat=$this->input->post('latD');
		$long=$this->input->post('longD');

		$result = $this->localizacion->get_loc($lat,$long);
		if(count($result)>0){
			print_r("Localizacion existente");
		}else{
			$resulLoc = $this->localizacion->nuevo_loc($lat,$long,"","","","");
		}
		$resulUs = $this->usuario->actualizar_usuarioLoc($id,$lat,$long);
		if($resulUs==true){

			$data = array(
				'todoOK' => true,                
				'message' => 'Modificado correctamente'
			);
		}else{

			$data = array(
				'error' => true,                
				'message' => 'Error al intentar registrar la Usuario'
			);
		}
	}

	public function actualizarDatos(){
		$this->load->model('usuarios_model','usuario');
		$this->load->model('personas_model','persona');

		$id=$this->input->post('idD');
		$email=$this->input->post('emailD');
		$nombre=$this->input->post('nombreD');
		$apellido=$this->input->post('apellidoD');
		$telefono=$this->input->post('telefonoD');
		
		$resulPer = $this->persona->actualizar_persona($id,$nombre,$apellido,$telefono);
		if($resulPer==true){

			$data = array(
				'todoOK' => true,                
				'message' => 'Modificado correctamente'
			);
		}else{

			$data = array(
				'error' => true,                
				'message' => 'Error al intentar registrar la Persona'
			);
		}
		
		$resulUs = $this->usuario->actualizar_email($id,$email);
		if($resulUs==true){

			$data = array(
				'todoOK' => true,                
				'message' => 'Modificado correctamente'
			);
		}else{

			$data = array(
				'error' => true,                
				'message' => 'Error al intentar registrar la Usuario'
			);
		}
		$this->enviarMail($email);
		
		if ($_SESSION['email']!='admin@gmail.com') {
			$_SESSION['email'] = $email;
		}

	}

	public function cerrarSesion(){
		session_destroy();
		$this->load->view('formulario_login');
	}
	public function nuevo_login(){
		$email = $this->input->post('email'); 
		$pass = $this->input->post('password');

		$this->load->model('usuarios_model','usuario');

		$arr_usuario=$this->usuario->get_usuario_by_email($email);

		if(count($arr_usuario)>0){

			foreach ($arr_usuario as $row)
			{
				if (password_verify($pass, $row->pass)) {
					$_SESSION['loggedin'] = true;
					$_SESSION['start'] = time();
					$_SESSION['expire'] = $_SESSION['start'] + (30 * 60) ;
					$_SESSION['email'] = $row->email;
					$_SESSION['id'] = $row->id;	
					redirect('/usuarios/inicio', 'refresh');
				}else{
					$data = array(
						'success' => true,                
						'message' => 'Contraseña incorrecta o usuario inexistente'
					);
					$this->load->view('formulario_login',$data);
				}
			}			
		}// NEW
		else{
			$data = array(
				'success' => true,                
				'message' => 'Contraseña incorrecta o usuario inexistente'
			);
			$this->load->view('formulario_login',$data);
		}
	}

	public function nuevo_registro(){

            // Usuario //
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$latitud = $this->input->post('lat');
		$longitud = $this->input->post('long');

            // Localizacion //
		$latitud = $this->input->post('lat'); 
		$longitud = $this->input->post('long'); 
		$ciudad = $this->input->post('idciudad');
		$calle = $this->input->post('idcalle');
		$pais = $this->input->post('idpais');
		$provincia = $this->input->post('idprov');

            // Persona //
		$genero = $this->input->post('genero');
		$nombre = $this->input->post('nombre');
		$apellido = $this->input->post('apellido');
		$fecha_nac = $this->input->post('fecha');
		$tel = $this->input->post('telefono');
		$web = $this->input->post('web');


            // Hashing del pass //
		$passHash = password_hash($pass, PASSWORD_DEFAULT);

            //carga el modelo de usuarios y ejecuta el metodo para insertar un usuario en la DB
		$this->load->model('usuarios_model','usuario');


            // Nueva Loc //
		$this->load->model('localizaciones_model','localizacion');

		$resulLoc=$this->localizacion->nuevo_loc($latitud,$longitud,$ciudad,$calle,$pais,$provincia);
		if($resulLoc==true){

			$data = array(
				'success' => true,                
				'message' => 'Localizacion registrada correctamente'
			);
		}else{

			$data = array(
				'error' => true,                
				'message' => 'Error al intentar registrar la Localizacion'
			);
		}

            // Nuevo usuario//
            //valida el email que no exista antes de insertar
		$arr_usuario=$this->usuario->get_usuario_by_email($email);
		$data_usuario; $pagina_retorno;
		if(count($arr_usuario)>0){

			$data = array(
				'warning' => true,                
				'message' => 'Ya existe un usuario registrado con el correo ingresado'
			);

			$this->load->view('formulario_registro',$data);
		}else{
			$resul=$this->usuario->nuevo_usuario($email,$passHash,$latitud,$longitud);
                //si el resultado del insert retorno true, quiere decir que se agrego correctamente el usuario
			if($resul==true){
				$data_usuario = array(
					'success' => true,                
					'message' => 'Usuario registrado correctamente'
				);
				$pagina_retorno = "formulario_login"; 
				//Si fue exitoso le mando al login papu
				if (isset($_SESSION['loggedin'])) {
					if ($_SESSION['email']=='admin@gmail.com') {
						$pagina_retorno="index";
					}
				}
				

			}else{
				$data_usuario = array(
					'error' => true,                
					'message' => 'Error al intentar registrar el usuario'
				);

				//Ni ahirton, inscribite bb
				
				$pagina_retorno = "formulario_registro";
			}

		}
		$id=$this->db->insert_id();

        // Nueva Persona //
		$this->load->model('personas_model','persona');
		$resulPer=$this->persona->nueva_persona($id,$nombre,$apellido,$fecha_nac,$web,$tel,$genero);
		if($resulPer==true){

			$data = array(
				'success' => true,                
				'message' => 'Registrado correctamente'
			);
		}else{

			$data = array(
				'error' => true,                
				'message' => 'Error al intentar registrar la Persona'
			);
		}

		// Enviar email //
		$data["mail"] = $this->enviarMail($email);

		//Envio la pagina indicada
		$this->load->view($pagina_retorno,$data);

	}

	private function enviarMail($email){
		//Se prepara el TOKEN
		//Para ello consigo el objeto que tiene los datos de la DB.
		$this->load->model('usuarios_model','usuario');

		//Obtengo uno en particular de acuerdo al email.
		$miUsuario = $this->usuario->get_usuario_by_email($email);

		//Creo las variables para ocuparlas luego.
		$token; 
		$idUsuario;

		//Obtengo la contraseña
		foreach ($miUsuario as $row){
			$token = $this->obtenerToken($row->id);
			$idUsuario = $row->id;
		}

		//Se prepara el cuerpo del msj.
		$subject = "Validar email";
		$headers = 'From: Taller AppWeb';
		$message = "Se ha registrado recientemente a MapClient, se requiere que valide su email mediante el siguiente link: \n\n localhost/app/usuarios/validar_emailDos/{$idUsuario}/{$token}";
		
		//Envio el mail
		mail($email,$subject,$message,$headers);

		return $message;
	}

	private function obtenerToken($unValorNumerico){
		return $unValorNumerico + 2;
	}

	public function validarToken($idUsuario, $unToken){
		$validar = false;

		if($unToken - 2 == $idUsuario){
			$validar = true;
		}

		return $validar;
	}

	public function listar(){
		$this->load->model('usuarios_model','usuario');
		$this->load->model('personas_model','persona');
		$arr_per = $this->persona->get_persona_by_id($_SESSION['id']);
		$arr_us = $this->usuario->get_usuario_by_email($_SESSION['email']);

		$datos;

		foreach ($arr_per as $row1) {
			foreach ($arr_us as $row2) {
				$datos = array('id' => $row2->id, 'email' => $row2->email, 'nombre' => $row1->nombre, 'apellido' => $row1->apellido, 'telefono' => $row1->tel, 'latitud' => $row2->latitud, 'longitud' => $row2->longitud);
			}
		}

		echo json_encode($datos);
	}

	public function validar_email(){

            //obtiene las variables que se enviar por el formulario de registro via metodo POST
		$email = $this->input->post('email');

            //carga el modelo de usuarios y ejecuta el metodo para insertar un usuario en la DB
		$this->load->model('usuarios_model','usuario');

		$arr_usuario=$this->usuario->get_usuario_by_email($email);
		if(count($arr_usuario)>0){

			$data = array(
				'warning' => true,                
				'message' => 'Ya existe un usuario registrado con el correo ingresado'
			);

		}else{

			$data = array(
				'success' => true,                
				'message' => 'Correo valido'
			);                
		}

		echo json_encode($data);

	}
	public function validar_email_dialog(){

            //obtiene las variables que se enviar por el formulario de registro via metodo POST
		$email = $this->input->post('email');

            //carga el modelo de usuarios y ejecuta el metodo para insertar un usuario en la DB
		$this->load->model('usuarios_model','usuario');
		
		if ($_SESSION['email']!=$email) {
			$arr=$this->usuario->get_usuario_by_email($email);
			if(count($arr)>0){

				$data = array(
					'warning' => true,                
					'message' => 'Ya existe un usuario registrado con el correo ingresado'
				);

			}else{

				$data = array(
					'success' => true,                
					'message' => 'Correo valido'
				);                
			}
		}else{
			$data = array(
				'igual' => true,                
				'message' => 'Correo valido'
			);
		}

		echo json_encode($data);

	}
	public function validar_emailDos($idUsuario, $token){
		$this->load->model('usuarios_model','usuario');
		if($this->validarToken($idUsuario, $token)){

			$this->usuario->validar_estado($idUsuario);
			$data = array(
				'success' => true,                
				'message' => 'Se ha validado correctamente xD'
			);
			$this->load->view('index',$data);
		}else{
			echo "Terrible travesti";
		}
	}

	public function crearRutaCarpetas(){
		$idUsuario =  $this->input->post('idUsuario');

		/* Creo el directorio necesario */
		$carpeta = "C:/xampp/htdocs/app/ckfinder/userfiles/images/usuario_$idUsuario";

		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		echo json_encode("images/usuario_$idUsuario/direccion_1");
	}
}
