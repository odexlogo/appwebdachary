<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="keywords" content="HTML,CSS,JavaScript,PHP">
    <meta name="author" content="Cribb Joel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  

    <script src="<?php echo base_url().'/js/leaflet/leaflet.js' ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url().'/js/leaflet/leaflet.css' ?>">

    <link rel="stylesheet" href="<?php echo base_url().'css/style.css' ?>">
    <script src="<?php echo base_url().'/js/jquery-3.4.1.js' ?>"></script>
    <script src="<?php echo base_url().'/js/myscript.js' ?>"></script>
</head>
<body id="index">
    <?php if( isset($warning) && $warning==true){ ?>
        <label class="warning"><?php echo $message; ?></label>
    <?php } ?>    

    <?php if( isset($success) && $success==true){ ?>
        <label class="success"><?php echo $message; ?></label>
    <?php } ?>

    <?php if( isset($error) && $error==true){ ?>
        <label class="error"><?php echo $message; ?></label>
    <?php } ?>

    <?php if(isset($mail)){ ?>
        <br><label class="success">Por no utilizar un servicio de Hosting, te proveemos el cuerpo del mensaje aqui:</label><br>
        <label><?php echo $mail; ?></label>
    <?php } ?>

    <a class="" href="<?php echo base_url().'usuarios/inicio' ?>"><h1>MapClient<sub>&reg;</sub></h1></a>
    <nav id="divlinks">
        <ul id="links">
            <a href="<?php echo base_url().'usuarios/registrar' ?>"><b>Crear una Cuenta</b></a> 
            <b> | </b> 
            <a href=""><b>Olvid&eacute; mi Contrase&ntilde;a</b></a>
            <b> | </b>
            <a href=""><b>Acerca de Nosotros </b></a>
        </ul>
    </nav>
    <br>
    <div id="login">
        <form class="loginclass" method="POST" action="<?php echo base_url().'usuarios/nuevo_login' ?>">
            <h2>Email</h2>
            <input class="inputlog" type="email" name="email" required>
            <h2>Contrase&ntilde;a</h2>
            <input class="inputlog" type="password" name="password" required><br><br>
            <input class="botones" id="botones" type="submit" name="iniciar" value="Iniciar sesion">
        </form>
    </div>
    <div>
        <img class="imageClick" src="<?php echo base_url().'css/img/hide1.png' ?>">
    </div>
    
</body>
<br>
<footer id="fut">
    <a href="https://www.openstreetmap.org/" target="_blank"><b>OpenStreetMap</b></a>
    <b> - </b>
    <a href="https://www.ugd.edu.ar/" target="_blank"><b>U.G.D.</b></a>
    <b> - </b>
    <a href="https://campusvirtual.ugd.edu.ar/moodle/login/index.php" target="_blank"><b>Campus Virtual</b></a>
</footer>
</html>