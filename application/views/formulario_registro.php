<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SESSION['loggedin'])) {
    if ($_SESSION['email']!='admin@gmail.com') {
        session_destroy();
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Crear Usuario</title>
    <meta charset="UTF-8">
    <meta name="keywords" content="HTML,CSS,JavaScript,PHP">
    <meta name="author" content="Cribb Joel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
    integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
    crossorigin=""></script>  
    <link href="https://unpkg.com/leaflet-geosearch@latest/assets/css/leaflet.css" rel="stylesheet" />
    <script src="https://unpkg.com/leaflet-geosearch@latest/dist/bundle.min.js"></script>

    <script src="<?php echo base_url().'/js/leaflet/leaflet.js' ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url().'/js/leaflet/leaflet.css' ?>">

    <link rel="stylesheet" href="<?php echo base_url().'css/style.css' ?>">
    <script src="<?php echo base_url().'/js/jquery-3.4.1.js' ?>"></script>
    <script src="<?php echo base_url().'/js/myscript.js' ?>"></script>

</head>
<body>

    <?php if( isset($warning) && $warning==true){ ?>
        <label class="warning"><?php echo $message; ?></label>
    <?php } ?>    

    <?php if( isset($success) && $success==true){ ?>
        <label class="success"><?php echo $message; ?></label>
    <?php } ?>

    <?php if( isset($error) && $error==true){ ?>
        <label class="error"><?php echo $message; ?></label>
    <?php } ?>    


    <body class="registro" id="index">
        <h1>Registrar usuario</h1>
        <nav id="divlinks">
            <ul id="links">
                <a href="<?php echo base_url().'usuarios/login' ?>"><b>Iniciar sesi&oacute;n</b></a> 
                <b> | </b> 
                <a href=""><b>Olvid&eacute; mi Contrase&ntilde;a</b></a>
                <b> | </b>
                <a href=""><b>Acerca de Nosotros </b></a>
            </ul>
        </nav>

        <div class="right">
            <img id="bannerlateral" src="<?php echo base_url().'css/img/2.png' ?>">
            <div id="caja">
                <p id="bannertext">Al hacer click en "Crear mi cuenta" acept&aacute;s las Condiciones y confirm&aacute;s que le&iacute;ste nuestra Pol&iacute;tica de datos, incluido el uso de cookies.</p>
            </div>

        </div>

        <div class="formulario">
            <h4>Datos de inicio de sesion</h4>
            <form method="POST" action="<?php echo base_url().'usuarios/nuevo_registro' ?>">
                <div class="row">
                    <div class="id60">
                        <label for="idemail">E-mail</label>
                    </div>
                    <div class="id30">
                        <img id="comprobar" >
                        <input id="idemail" type="email" name="email" onblur="validad_usuario()" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idpass">Contrase&ntilde;a</label>
                    </div>
                    <div class="id30">
                        <input id="idpass" type="password" name="pass" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idre">Repita contrase&ntilde;a</label>
                    </div>
                    <div class="id30">
                        <input id="idre" type="password" name="repass" required><br>
                    </div>
                </div>
                <h4>Datos personales</h4>
                <div class="row">
                    <div class="id60">
                        <label for="idnombre">Nombre</label>
                    </div>
                    <div class="id30">
                        <input id="idnombre" type="text" name="nombre" maxlength="60"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idapellido">Apellido</label>
                    </div>
                    <div class="id30">
                        <input id="idapellido" type="text" name="apellido" maxlength="60"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idgenero">G&eacute;nero</label>
                    </div>
                    <div class="id30">
                        <label>Masculino </label><input id="genero" type="radio" name="genero" value="Masculino">
                        <label>Femenino </label><input id="genero" type="radio" name="genero" value="Femenino"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idtelefono">N&uacute;mero de Tel&eacute;fono</label>
                    </div>
                    <div class="id30">
                        <input id="idtelefono" type="text" name="telefono"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idfecha">Fecha de Nacimiento</label>
                    </div>
                    <div class="id30">
                        <input id="idfecha" type="date" name="fecha"><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idpagina">P&aacute;gina Web</label>
                    </div>
                    <div class="id30">
                        <input id="idpagina" type="text" name="web"><br>
                    </div>
                </div>
                <h4>Datos de localizaci&oacute;n</h4>
                <div class="row">
                    <div class="id60">
                        <label for="idpais">Pa&iacute;s</label>
                    </div>
                    <div class="id30">
                        <input id="idpais" type="text" name="idpais" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idprov">Provincia/Estado</label>
                    </div>
                    <div class="id30">
                        <input id="idprov" type="text" name="idprov" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idciudad">Ciudad</label>
                    </div>
                    <div class="id30">
                        <input id="idciudad" type="text" name="idciudad" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="idcalle">Calle</label>
                    </div>
                    <div class="id30">
                        <input id="idcalle" type="text" name="idcalle" required><br>
                    </div>
                </div>
                <div class="row">
                    <div class="id60">
                        <label for="latlong">Coordenadas</label>
                    </div>
                    <div class="id30">
                        <label for="lat">Latitud</label><input id="latitud" type="text" name="latitud" disabled>
                        <input id="lat" type="text" name="lat" hidden>
                        <label for="long"> Longitud</label><input id="longitud" type="text" name="longitud" disabled>
                        <input id="long" type="text" name="long" hidden><br>
                    </div>
                </div>
                <div id="map"><fieldset class="mapid" id="mapid"></fieldset></div>

                <div class="id30">
                    <input class="botones" id="crear" type="submit" name="crear" value="Crear mi cuenta">
                </div>
            </form>
        </div>
    </body>
    <footer id="fut">
        <a href="https://www.openstreetmap.org/" target="_blank"><b>OpenStreetMap</b></a>
        <b> - </b>
        <a href="https://www.ugd.edu.ar/" target="_blank"><b>U.G.D.</b></a>
        <b> - </b>
        <a href="https://campusvirtual.ugd.edu.ar/moodle/login/index.php" target="_blank"><b>Campus Virtual</b></a>

    </footer>

    </html>