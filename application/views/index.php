<!DOCTYPE html>
<html>
<head>
	<title>Inicio</title>
	<meta charset="UTF-8">
	<meta name="keywords" content="HTML,CSS,JavaScript,PHP">
	<meta name="author" content="Cribb Joel">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">  

	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
	integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
	crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
	integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
	crossorigin=""></script>  
	<link href="https://unpkg.com/leaflet-geosearch@latest/assets/css/leaflet.css" rel="stylesheet" />
	<script src="https://unpkg.com/leaflet-geosearch@latest/dist/bundle.min.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<script src="<?php echo base_url().'/js/ckfinder/ckfinder.js' ?>"></script>

	<link rel="stylesheet" href="<?php echo base_url().'css/style.css' ?>">
	<script src="<?php echo base_url().'/js/jquery-3.4.1.js' ?>"></script>
	<script src="<?php echo base_url().'/js/jquery-ui-1.12.1/jquery-ui.js' ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url().'/js/leaflet/leaflet.css' ?>">
	<script src="<?php echo base_url().'/js/leaflet/leaflet.js' ?>"></script>
	<script src="<?php echo base_url().'/js/index.js' ?>"></script>
	<script src="<?php echo base_url().'/js/leaflet-underneath/dist/leaflet-underneath.js' ?>"></script>
</head>
<body id="index">
	<?php if( isset($success) && $success==true){ ?>
		<script>alert('Se ha validado correctamente')</script>
	<?php } ?>
	<?php if( isset($todoOK) && $todoOK==true){ ?>
		<label><?php echo $message; ?></label>
	<?php } ?>
	<?php if( isset($error) && $error==true){ ?>
		<label><?php echo $message; ?></label>
	<?php } ?>
	<?php 
	if (isset($_SESSION['loggedin'])) {  
	}	
	else {
		echo "<fieldset class='alerta'><h4 class='alertaH'>Necesitas loguear para acceder a esta pagina</h4>
		<strong><a href='http://localhost/app/usuarios/login'>Login</a></strong></fieldset>";

		exit;
	}
	    // Tiempo logueado
	$now = time();           
	if ($now > $_SESSION['expire']) {
		session_destroy();
		echo "<div class='alerta' role='alert'>
		<h4 class='alertaH'>Tu sesion ha experado</h4>
		<strong><a href='http://localhost/app/usuarios/login'>Login</a></strong></div>";
		exit;
	}
	?> 
	<nav class="nave" id="nave">

		<div class="titulo">
			<h1>MapClient<sub>&reg;</sub></h1>
		</div>
		

	</nav>
	<div id="dialog" title="Modificar cuenta" style="display: none;">
		<form method="POST" id="formD" action="<?php echo base_url().'usuarios/actualizarDatos' ?>">
			<input id="idD" name="idD" type="text" placeholder="Id" readonly>
			<input id="emailD" name="emailD" type="email" placeholder="Email" required onblur="validar_email_dialog()">
			<input id="nombreD" name="nombreD" type="text" placeholder="Nombre">
			<input id="apellidoD" name="apellidoD" type="text" placeholder="Apellido">
			<input id="telefonoD" name="telefonoD" type="text" placeholder="Telefono">
			<button id="update" name="update" type="submit" onclick="actualizarDatos()">Modificar</button>
		</form><!-- onclick="actualizarDatos()" -->

	</div>
	<div id="dialogLoc" title="¿Utilizar marcador actual como nueva localización?" style="display: none;">
		<form method="POST" id="locD" action="<?php echo base_url().'usuarios/actualizarLoc' ?>">
			<div class="labels" id="labels">
				<label for="iD">Usuario</label><br>
				<label for="latD">Latitud</label><br>
				<label for="longD">Longitud</label><br>
				<label for="idpais">Pais</label><br>
				<label for="idprov">Provincia</label><br>
				<label for="idciudad">Ciudad</label><br>
				<label for="idcalle">Calle</label><br>
			</div>
			<div class="labels" id="inputs">
				<input id="iD" name="iD" type="text" placeholder="Id" readonly>
				<input id="latD" name="latD" type="text" readonly>
				<input id="longD" name="longD" type="text" readonly>
				<input id="idpais" type="text" name="idpais" required>
				<input id="idprov" type="text" name="idprov" required>
				<input id="idciudad" type="text" name="idciudad" required>
				<input id="idcalle" type="text" name="idcalle" required>
			</div>
			<br>
			<button id="update" name="update" type="submit" onclick="actualizarLoc()">Modificar</button>
		</form>
	</div>
	<div class="dialogos" id="dialogU" title="Información del Usuario" style="display: none;">

		<div class="labels" id="labels">
			<label for="idU">Usuario</label><br>
			<label for="emailU">Email</label><br>
			<label for="nombreU">Nombre</label><br>
			<label for="apellidoU">Apellido</label><br>
			<label for="telU">Telefono</label><br>
			<label for="latU">Latitud</label><br>
			<label for="longU">Longitud</label><br>
			<label for="generoU">Genero</label><br>
			<label for="fecha_nacU">Fecha</label><br>
			<label for="webU">Web</label><br>
			<label for="idpaisU">Pais</label><br>
			<label for="idprovU">Provincia</label><br>
			<label for="idciudadU">Ciudad</label><br>
			<label for="idcalleU">Calle</label><br>
		</div>
		<div class="labels" id="labels2">
			<label id="idU" name="iD" type="text" placeholder="Id"></label><br>
			<label id="emailU" name="emailU" type="text" ></label><br>
			<label id="nombreU" name="nombreU" type="text" ></label><br>
			<label id="apellidoU" name="apellidoU" type="text" ></label><br>
			<label id="telU" name="telU" type="text" ></label><br>
			<label id="latU" name="latU" type="text"></label><br>
			<label id="longU" name="longU" type="text" ></label><br>
			<label id="generoU" name="generoU" type="text" ></label><br>
			<label id="fecha_nacU" name="fecha_nacU" type="text" ></label><br>
			<label id="webU" name="webU" type="text" ></label><br>
			<label id="idpaisU" type="text" name="idpaisU" ></label><br>
			<label id="idprovU" type="text" name="idprovU" ></label><br>
			<label id="idciudadU" type="text" name="idciudadU" ></label><br>
			<label id="idcalleU" type="text" name="idcalleU" ></label><br>
		</div>

	</div>

	<div class="div25" id="div25">
		<h5 class="logout" onclick="borrarMarkers()" >Borrar marcadores <img class="borrar" src="<?php echo base_url() . 'css/img/X.jpg'; ?>"></h5>
		<?php if ($_SESSION['email'] == 'admin@gmail.com') { ?>
			<a class="logout" id="nuevo_usuario" href="<?php echo base_url() . 'usuarios/registrar'; ?>"><h5>Nuevo Usuario <img class="new" src="<?php echo base_url() . 'css/img/mas.png'; ?>"></b5></a>
			<?php } ?>
			<a class="logout" href="<?php echo base_url() . 'usuarios/cerrarSesion'; ?>">Cerrar sesión <img class="new" src="<?php echo base_url() . 'css/img/logout.png'; ?>"></a>
		</div>

		<div id="indexmap"><fieldset class="mapid" id="mapid"></fieldset></div>

		<div class="usuario">
			<?php if ($_SESSION['email'] == 'admin@gmail.com') { ?>
				<h3>Lista de Usuarios</h3>
			<?php }else{ ?>
				<h3>Usuario</h3>
			<?php } ?>
			<div>
				<div id="tabla"></div>

			</div>	
		</div>

	</body>

	</html>